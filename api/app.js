const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const cors = require('cors')
const rp = require('request-promise')

const client_id = 'fdc846107e28457c9e95abc958459b56'
const client_secret = 'fafbdfec69ef4e4e9f97d32687dcc0e9'

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

// Donnée normalement en base de donnée
const trackInLibrary = []


app.post('/track', (req, res) => {
	if (req.body.id && !trackInLibrary.find((track) => {
		return track.id === req.body.id
	})) {
		trackInLibrary.push({
			id: req.body.id,
			fav: false,
			tags: new Set()
		})
	}
	res.json({})
})

app.post('/track/fav', (req, res) => {
	if (req.body.id && req.body.state) {
		const track = trackInLibrary.find(track => track.id === req.body.id)
		if (track) {
			track.fav = req.body.state
		}
	}
	res.json({})
})

app.post('/track/remove', (req, res) => {
	if (req.body.id) {
		const idx = trackInLibrary.findIndex((track => track.id === req.body.id))
		if (idx >= 0) {
			trackInLibrary.splice(idx, 1)
		}
	}
	res.json({})
})

app.post('/tag', (req, res) => {
	if (req.body.name && req.body.trackids) {
		trackInLibrary.forEach((track) => {
			console.log(req.body.trackids, track.id)
			if (req.body.trackids.includes(track.id)) {
				track.tags.add(req.body.name)
			}
		})
	}
	res.json({})
})


app.get('/tracks', (req, res) => {
	// Formatage Set -> Array
	const tracksFormated = trackInLibrary.map((track) => {
		return Object.assign({}, track, {tags: Array.from(track.tags)})
	})
	res.json({tracks: tracksFormated})
})

app.get('/token', async (req, res) => {
	const options = {
		method: 'POST',
		url: 'https://accounts.spotify.com/api/token',
		headers: {
			'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
		},
		form: {
			grant_type: 'client_credentials'
		},
		json: true
	}
	const data = await rp(options)
	res.json({token: data.access_token})
})

app.listen(3000, () => {
	console.log('Node listen on port 3000')
})

